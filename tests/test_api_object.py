import pytest
from mimecast_api import MimecastAPI, MimecastAPIFail

@pytest.fixture
def api():
    m = MimecastAPI(base_url='http://example.com/',
                    access_key='YWNjZXNz',
                    secret_key='c2VjcmV0',
                    app_id='totally-a-uuid-with-dashes',
                    app_key='YXBwa2v5')
    return m

@pytest.fixture
def fail_json():
    # this is not really a plausible failure for url decoding or mail
    # searching - i copied it from the documentation about api
    # failures.
    # https://www.mimecast.com/tech-connect/documentation/api-overview/api-concepts/
    # but yes, status should be 200 for a failure. this is not a copy-paste bug.
    return {'meta': {'status': 200},
            'data': [],
            'fail': [{'key': {'domain': 'nonexistent.domain.com'},
                      'errors': [
                          {'code': 'err_domain_not_found',
                           'message': 'domain not found',
                           'retryable': False}]}]}

def test_decode_happy(requests_mock, api):
    succeed = {'meta': {'status': 200},
               'data': [{'url': 'hi i am decoded'}],
               'fail': []}
    requests_mock.post('http://example.com/api/ttp/url/decode-url',
                       json=succeed)
    assert api.decode_url('http://mysterious-url') == 'hi i am decoded'


def test_decode_fail(requests_mock, api, fail_json):
    requests_mock.post('http://example.com/api/ttp/url/decode-url',
                       json=fail_json)
    with pytest.raises(MimecastAPIFail):
        api.decode_url('http://mysterious-url')


def test_recent_recipients_happy(requests_mock, api):
    # https://www.mimecast.com/tech-coanect/documentation/endpoint-reference/message-finder-formally-tracking/search/
    succeed = {"fail": [],
               "meta": {"status": 200},
               "data": [{
                   "trackedEmails": [{
                       # [lots of other info about a message elided]
                       "to": [{"displayableName": "Contact",
                               "emailAddress": "contact@us.example.com"}],}]}]}
    requests_mock.post('http://example.com/api/message-finder/search',
                       json=succeed)
    who = api.list_recent_recipients_from('malice@evil.example.com', 2)
    assert who == ['contact@us.example.com']


def test_recent_recipients_happy(requests_mock, api, fail_json):
    requests_mock.post('http://example.com/api/message-finder/search',
                       json=fail_json)
    with pytest.raises(MimecastAPIFail):
        who = api.list_recent_recipients_from('malice@evil.example.com', 2)


def test_is_url_managed_happy_managed(requests_mock, api):
    succeed = {"fail": [],
               "meta": {"status": 200},
               "data": [{
                   "matchType": "explicit",
                   "queryString": "",
                   "scheme": "https",
                   "domain": "blocked.example.com",
                   "path": "/blocked-path",
                   "action": "turn all images upside down",
                   "comment": "foo",
               }]}
    requests_mock.post('http://example.com/api/ttp/url/get-all-managed-urls',
                       json=succeed)
    assert True == bool(api.is_url_managed('https://blocked.example.com/blocked-path'))


def test_is_url_managed_happy_not_managed(requests_mock, api):
    requests_mock.post('http://example.com/api/ttp/url/get-all-managed-urls',
                       json={"fail": [],
                             "meta": {"status": 200},
                             "data": []})
    assert False == bool(api.is_url_managed('https://blocked.example.com/blocked-path'))


def test_is_url_managed_fail(requests_mock, api, fail_json):
    requests_mock.post('http://example.com/api/ttp/url/get-all-managed-urls',
                       json=fail_json)
    with pytest.raises(MimecastAPIFail):
        api.is_url_managed('https://blocked.example.com/blocked-path')
