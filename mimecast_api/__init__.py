from .errors import MimecastAPIFail, URLDecodeFail
from .api_object import MimecastAPI
