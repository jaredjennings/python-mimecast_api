Mimecast API access
-------------------

Mimecast is a company providing email security and other
services. Customers can access some of these services via Mimecast's
"post-REST" API. This unofficial client library starts from the
example Python code in Mimecast's documentation, and adds a thin layer
on top.

See also https://www.mimecast.com/tech-connect/documentation/
